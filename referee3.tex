\reviewer{3}

\comment{
This article introduces a framework for expressing pose uncertainty in
the Lie Algebra. Therefore, the authors describe how uncertainty can be
characterized in the Lie Algebra, and introduce methods for pose
operations using this formulation, such as pose composition or
inversion, that are commonly used in pose-graph SLAM. All operations
take into account correlations between poses instead of assuming
independent poses, which is usually not the case in pose-graph SLAM.
The authors quantify the improvement of the uncertainty estimation
experimentally using simulated cases and real-world data sets.

The main contribution of this article is the formulation of pose
uncertainty, including the common pose operations pose composition,
pose inversion and relative pose extraction, for Lie groups. Compared
to existing works in the literature, the authors thereby do not assume
independence between the poses, which is usually not the case in SLAM,
but model the uncertainty framework for jointly correlated poses. The
improvements in terms of accuracy of the estimated uncertainty is
evaluated in simulation on pose-pairs as well as on a real-world SLAM
data set. Furthermore, the authors describe how to convert to the
presented uncertainty parametrization from other representations and
release an accompanying C++ library implementation.

This article is generally well written and presented, the explanations
are mostly clear and transparent to follow, and the described method is
theoretically well-founded. The contributions of the article are
clearly stated and supported by the description of the framework and
the experiments.}

\response{Thank you for your careful summary of our paper and kind comments.}

\comment{The main criticism of this reviewer refers
to the presented experimental results on real-word data, which could,
to this reviewer's opinion, be better described and also extended to
strengthen the contribution of this article.}

\response{Thank you for your very helpful critique. We agree and have revised
  Section X-B as follows to improve the experimental description:

  \begin{excerpt}
  To evaluate \ReviewEdit{uncertainty propagation through the} relative pose \ReviewRm{extraction}\ReviewEdit{operation}, we used \ReviewRm{iSAM [34] to find a solution to} the Manhattan 3500 dataset [4] \ReviewEdit{to generate a set of pose pairs on which to perform the relative pose operation}. \ReviewEdit{After using iSAM [34] to find a solution to the SLAM problem, we} \ReviewRm{We then} 
extracted joint mean and covariance as described in Section IX-B for pairs of poses at offsets varying from 5 to 50 in increments of 5 as well as for offsets of 100, 200, and 500 nodes. A visualization of the extracted pose pairs and the correlation between them for offsets of 50 are shown in Fig. 10 (a-c). \ReviewEdit{For each pose pair, we plot a line between the two poses colored according to the degree of correlation between the two poses with respect to the $x$, $y$, and $\theta$ dimensions.} Equivalent visualizations for offsets of 10, 100, 200, and 500 are shown in Fig. 11, Fig. 12, Fig. 13, and Fig. 14.

\ReviewEdit{For each extracted pose pair, we performed a Monte Carlo simulation to determine a ground truth relative pose distribution against which we could compare the first-order propagation methods being investigated. First, the mean and joint marginal covariance for each pose pair was extracted from the iSAM solution. Then, for a given pair, a large number of sample pose pairs were generated from the extracted pose pair distribution and the relative pose between each of those pairs was evaluated. This gave us a sample distribution of the relative transformation between the two poses in the original pair which was used as groundtruth. We then repeated the process for each joint pose pair distribution extracted from the dataset.}

\ReviewEdit{Using this Monte Carlo derived relative pose covariance as a reference, we can use the following covariance error metric to evaluate the predicted covariance for the various methods we compare:}
\begin{equation}
  \ReviewEdit{\epsilon \triangleq \sqrt{\operatorname{tr}\left( (\mathbf{\Sigma} - \mathbf{\Sigma}_{mc})^\top (\mathbf{\Sigma} - \mathbf{\Sigma}_{mc}) \right)}, \label{eq:cov_error}}
\end{equation}
\ReviewEdit{where $\mathbf{\Sigma}_{mc}$ is the sample relative pose covariance matrix generated via Monte Carlo for the given pose.}

\ReviewRm{To investigate the importance of taking into account correlation when estimating relative pose, we performed a Monte Carlo simulation to estimate the true relative pose covariance, where $\boldsymbol{\xi}_{g1}^{m}$ and $\boldsymbol{\xi}_{g2}^{m}$ are the perturbation variables sampled from the extracted joint covariance and $\bar{\mathbf{T}}_{g1}$ and $\bar{\mathbf{T}}_{g2}$ are the mean values extracted from the iSAM solution. We then used the metric defined in \eqref{eq:cov_error} to}\ReviewEdit{To} evaluate \ReviewRm{the covariance error of} our method proposed in (51) when taking into account correlation (using all four terms in (51)) or ignoring correlation (using only the first two terms in (51))\ReviewEdit{, we used both methods to propagate pose uncertainty through the relative pose transformation for each pair and evaluated the error of the predicted covariance according to (60)}. Summary statistics are shown in Table I and visualizations of the error for offsets of 50, 10, 100, 200, and 500 are shown in (d-e) of Fig. 10, Fig. 11, Fig. 12, Fig. 13, and Fig. 14.
\end{excerpt}
}


\comment{Regarding the evaluation of the proposed framework, this reviewer
especially appreciates the fact that the authors evaluate the effect of
taking into account correlations vs. the assumption of independent
poses, before moving on to a 'full SLAM example'. However, regarding
the experiment on SLAM data (Sec. IX-B), this reviewer finds it quite
hard to follow the explanations of the authors on the design as well as
on the evaluation metrics of the conducted experiment. This reviewer
would suggest revising this section to improve transparency.}

\response{Thank you for your suggestion.

  We have revised the description of Sec. X-B as described in the response to the previous comment.}

\comment{Furthermore, why was the solution of iSAM used for the poses, while
there should be ground truth available for benchmarking data sets?}

\response{Thank you for your question.

  It is true that a ground truth
  pose could be obtained from the simulated data set, however the goal of
  the proposed method is not to find the final pose from the SLAM solution, but
  to correctly characterize the uncertainty of the relative pose for a given pair of
  poses extracted from a given SLAM solution. Thus, the mean of two poses
  as well as the joint covariance of the two poses was extracted from the iSAM
  solution and Monte-Carlo simulation was used to provide a ``ground-truth'' estimate
  of the relative pose distribution. We then compared the relative pose distribution
  predicted by our method and that proposed by Smith, Self, \& Cheeseman with the
  pose predicted by Monte-Carlo.}

\comment{While the experiments authentically substantiate the claim of the
authors that their framework results in more accurate uncertainty
evaluation, it would be interesting to see how this finally affects the
accuracy of the SLAM estimate, since from this reviewer's point of
view, the final benefit from better uncertainty estimation should be
increased accuracy of the estimate. Therefore, this reviewer would ask
the authors to quantify also RMSE accuracy for the SLAM solution
experiment.}

\response{Thank you for your comment.

  While the factor graph
  framework used to formulate the Pose-Graph SLAM problem is affected by the
  modeled uncertainty of the individual factors, the primary focus of the
  relative pose operation presented in this paper is not on improving the
  accuracy of the SLAM solution, but on accurately characterizing uncertainty
  when performing operations on the correlated pose estimates derived from the SLAM solution.
  For example, after solving the SLAM problem, it is often necessary to determine
  the relative pose between globally referenced poses estimated by the SLAM solution
  in order to evaluate the likelihood of a potential loop-closure.

  To make this more clear, we have added the following paragraph to the introduction:

  \begin{excerpt}
\ReviewEdit{In addition to accurately characterizing the uncertainty of a single pose, it is often necessary to propagate the uncertainty of one or more poses through non-linear operations such as pose composition, pose inversion, and relative pose extraction. For example, given an odometry sequence consisting of multiple uncertain relative pose transformations, it may be necessary to compose the entire sequence of transformations together to obtain a distribution describing the relative pose of the robot over an entire sequence. Alternatively, after estimating the pose of the robot at multiple time steps via maximum likelihood estimation (MLE) or SLAM, it can be useful to extract the relative pose of the robot between two time steps to evaluate whether a potential loop-closure is consistent with the estimated trajectory.}
  \end{excerpt}

}

\comment{Furthermore, additional experiments on a second real-world data set
would, to this reviewer's opinion, strengthen the contribution of the
article in terms of confirming that the benefits from this framework
not only apply in simulation, but also in real-world examples. However,
the evaluation does not have to be as extensive as for the first
experiments, the authors could focus only on the most important aspects
here.}

\response{We thank you for your suggestion and have extended
  the evaluation by performing the following experiment as decribed in Section X-C:

%  \quote{
 \begin{excerpt}

    \ReviewEdit{ The results of the previous experiment show that our proposed method results in more accurate characterizations
  of pose uncertainty when used to evaluate the relative pose between pairs of poses extracted from an estimated SLAM solution.
  This increased accuracy is important when evaluating potential loop-closures. To emphasize this, we performed an additional
  experiment on the CSAIL real-world dataset collected at MIT [42].}


\ReviewEdit{We first divided the factors in the graph into a group of verified accurate loop-closures and odometry. We then
  used iSAM [34] to estimate a solution to the graph using odometry only and extracted the joint block covariance
  for the pairs of nodes associated with each loop-closure. As before, we extracted this joint covariance twice using both the
  proposed parameterization and that used by SSC. We then estimated the relative pose between the two base poses using the
  method proposed in Section VIII and via the SSC tail-to-tail operation and evaluated the proposed loopclosures
  by thresholding the squared Mahalanobis distance.}


\ReviewEdit{For a given loop closure $\mathbf{T}_{jk}$, the SSC Mahalanobis distance was evaluated by applying the SSC tail-to-tail
operation to the mean and associated joint covariance for the two poses $\mathbf{x}_{gj}$ and $\mathbf{x}_{gk}$ extracted from
iSAM to obtain $\hat{\mathbf{x}}_{jk}^{odom}$ and $\mathbf{\Sigma}_{jk}^{odom}$ and then calculating the distance as follows:}
\begin{align}
  \ReviewEdit{\delta_{SSC} = (\hat{\mathbf{x}}_{jk}^{odom} - \mathbf{x}_{jk})^\top \mathbf{\Sigma}_{jk}^{odom^{-1}} (\hat{\mathbf{x}}_{jk}^{odom} - \mathbf{x}_{jk}),}
\end{align}
\ReviewEdit{where $\mathbf{x}_{jk}$ is $\mathbf{T}_{jk}$ expressed as a parameter vector in $\mathbb{R}^3$ as defined in [5] for $\mathrm{SO}(2)$.}


\ReviewEdit{Under the proposed parameterization, the Mahalanobis distance calculation was performed by using the relative pose operation defined in Section VIII to estimate $\bar{\mathbf{T}}_{jk}^{odom}$ and $\mathbf{\Sigma}_{jk}^{odom}$ from the poses $\mathbf{T}_{gj}$ and $\mathbf{T}_{gk}$ and their joint covariance matrix extracted from iSAM and then calculating the distance as follows:}
\begin{align}
  \ReviewEdit{\delta_{Lie} = \operatorname{log}(\mathbf{T}_{jk} \bar{\mathbf{T}}_{jk}^{odom^{-1}})^\top \mathbf{\Sigma}_{jk}^{odom^{-1}} \operatorname{log}(\mathbf{T}_{jk} \bar{\mathbf{T}}_{jk}^{odom^{-1}})}.
\end{align}


\ReviewEdit{Finally, both distances where compared against the chi-squared threshold value 7.814 corresponding to a 95 percent likelihood for three degrees of freedom. The results are shown in Fig. 15, Fig.16, and Table III.}


\ReviewEdit{When using the proposed Lie algebra based method, 100 percent of the verified loop-closures fall within the expected chi-squared bounds, while when using SSC more than ten percent fall outside it (see Fig. 16 and Table III)}.

\end{excerpt}

}


\comment{To this authors experience from other articles, '\textsection~VII' is not a common
  abbreviation for 'Section VI'. Please consider using e.g. 'Sec. IV'.}

\response{Throughout the paper, all section references were changed from \ReviewRm{\textsection} $\rightarrow$ \ReviewEdit{Section}.}

\comment{P2: typo: non-linearm}

\response{\ReviewRm{non-linearm} $\rightarrow$ \ReviewEdit{non-linear}}

\comment{Eq. 35: 'E' is not introduced}

\response{We thank the reviewer for pointing out this omission. We
  made the following changes to the manuscript to resolve this issue:

  \begin{excerpt}
Computing the covariance $\mathbf{\Sigma}$ amounts to evaluating $E[\xib{ik}\xib{ik}^\top]$, \ReviewEdit{where $E[\cdot]$ signifies the expected value of a given random variable}.
\end{excerpt}
}

\comment{Fig. 4: Color scheme is not optimal (color for ellipse for
independent solution does not match legend, green ellipse on green
ellipse). Furthermore, it took this reviewer a moment to understand the
plot - it is not directly clear that the black lines are trajectories
from the same starting point. Maybe the caption could be revised to
improve clarity.}

\response{We thank the reviewer for pointing this out. We have changed the caption to Fig. 4 as
  follows to clarify the figure both in terms of general understanding and color scheme:

  \begin{excerpt}
  Plots of 10000 sample trajectories \ReviewEdit{(shown in black), each} made up of \ReviewEdit{a sequence of }10 noisy pose transformations \ReviewEdit{starting from the same point}. The 95\% likely uncertainty ellipse predicted by first order uncertainty propagation through the SSC head-to-tail operation is shown in red \ReviewEdit{overlayed on top of the sample trajectories}, while a representation of the flattened 95\% likely uncertainty position ellipsoids predicted by the Lie algebra pose composition methods when correlation is and is not taken into account are \ReviewRm{shown in }\ReviewEdit{overlayed in transparent} green and cyan, respectively.
  \end{excerpt}
}

\pagebreak

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "response"
%%% End:
